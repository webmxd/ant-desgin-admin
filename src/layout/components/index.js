export { default as Sider } from "./Sider/index.vue";
export { default as Navbar } from "./Navbar/index.vue";
export { default as TabsView } from "./TabsView/index.vue";
export { default as AppMain } from "./AppMain/index.vue";
export { default as Footer } from "./Footer/index.vue";
