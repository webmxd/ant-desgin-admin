import { createApp } from 'vue'
import App from './App.vue'
// 引入路由
import router from './router'
// 引入状态管理仓库
import store from './store'

// 全局引入浏览器捕获机制
import 'default-passive-events';

// 引入antDesgin 组件库
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/reset.css'

// 引入VXETable表格组件
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

// 引入svg icon注册脚本
import 'virtual:svg-icons-register'

// 引入公共样式库
import '@/styles/index.scss'

// 引入路由守卫
import '@/permission.js'

// 引入字体图标
import '@/assets/iconfont/iconfont.css'

// 引入全局指令
import * as directives from "@/directives/index.js";

// 引入全局组件
import SvgIcon from "@/components/SvgIcon/index.vue"
import ColorIcon from "@/components/ColorIcon/index.vue"
import PageSearch from "@/components/PageSearch/index.vue"
import PageForm from "@/components/PageForm/index.vue";
import PageTitle from "@/components/PageTitle/index.vue";
import PageDetail from "@/components/PageDetail/index.vue";
import PageTable from "@/components/PageTable/index.vue";
import SelectIcon from "@/components/SelectIcon/index.vue";
import SelectArea from "@/components/SelectArea/index.vue";

const app = createApp(App)

app.use(router)
app.use(store)
app.use(Antd)
app.use(VXETable)
app.component('SvgIcon', SvgIcon)
app.component('ColorIcon', ColorIcon)
app.component('PageSearch', PageSearch)
app.component('PageForm', PageForm)
app.component('PageTitle', PageTitle)
app.component('PageDetail', PageDetail)
app.component('PageTable', PageTable)
app.component('SelectIcon', SelectIcon)
app.component('SelectArea', SelectArea)
app.mount('#app')

//注册自定义指令
Object.keys(directives).forEach((key) => {
    //Object.keys() 返回一个数组，值是所有可遍历属性的key名
    app.directive(key, directives[key]); //key是自定义指令名字；后面应该是自定义指令的值，值类型是string
});
