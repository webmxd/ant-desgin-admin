import { defineStore } from 'pinia'
import Cookies from "js-cookie";
//页面设置信息
const useAppStore = defineStore(
    'app',
    {
        state: () => ({
            sider: {
                // 侧边栏是否展开
                opened: Cookies.get("siderStatus")
                    ? !!+Cookies.get("siderStatus")
                    : true // siderStatus代表的状态：1：展开，0：关闭
            },
            device: "desktop" // 设备类型，桌面：desktop；手机：mobile；平板：tablet
        }),
        persist: false,
        actions: {
            toggleSideBar() {
                this.sider.opened = !this.sider.opened
                if (this.sider.opened) {
                    Cookies.set('siderStatus', 1)
                } else {
                    Cookies.set('siderStatus', 0)
                }
            },
            showSideBar() {
                Cookies.set("siderStatus", 1);
                this.sider.opened = true;
            },
            closeSideBar() {
                Cookies.set('siderStatus', 0)
                this.sider.opened = false
            },
            toggleDevice(device) {
                this.device = device
            },
        }
    })

export default useAppStore


