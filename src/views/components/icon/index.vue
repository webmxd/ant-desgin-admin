<!--
功能：图标列表
作者：Maoxiangdong
邮箱：862755519@qq.com
时间：2023-11-21 08:44:37
-->
<template>
  <div>
    <PageTitle
      title="字体图标"
      subTitle="字体图标引用，使用html标签然后class类名设置iconfont 图标名，例如：<i class='iconfont xiazai1'/>"
    ></PageTitle>
    <div class="page-content">
      <a-input-search
        v-model:value="keyword"
        placeholder="输入图标名称进行搜索"
        enter-button
        style="width: 400px"
        @search="handleSearch"
      />
      <div class="icon-container">
        <div class="icon-item" v-for="(item, index) in iconList" :key="index">
          <i class="iconfont xiazai1" :class="item.font_class" style="font-size: 30px"></i>
          <div class="icon-name">{{ item.name }}</div>
          <div class="icon-code">{{ item.font_class }}</div>
          <div class="icon-footer">
            <span @click="handleCopyCode(item)">复制代码</span>
            <a-divider type="vertical" />
            <span @click="handleCopyClassName(item)">复制类名</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script setup>
import iconData from '@/assets/iconfont/iconfont.json'
import clipboard3 from 'vue-clipboard3'
import { ref, onMounted } from 'vue'
import { message } from 'ant-design-vue'
// 图标数据
const iconList = ref([])
// 搜索关键词
const keyword = ref(null)
// 搜索
const handleSearch = () => {
  if (keyword.value) {
    const filteredArray = iconData.glyphs.filter((item) => item.name.includes(keyword.value))
    iconList.value = filteredArray
  } else {
    iconList.value = iconData.glyphs
  }
}
// 解构出复制方法
const { toClipboard } = clipboard3()
// 复制代码
const handleCopyCode = async (event) => {
  try {
    const copyText = `<i class="iconfont ${event.font_class}"/>`
    await toClipboard(copyText)
    message.success('代码已复制到剪贴板')
  } catch (error) {
    message.error('复制失败')
  }
}
// 复制类名
const handleCopyClassName = async (event) => {
  try {
    const copyText = event.font_class
    await toClipboard(copyText)
    message.success('类名已复制到剪贴板')
  } catch (error) {
    message.error('复制失败')
  }
}
// onMounted
onMounted(() => {
  iconList.value = iconData.glyphs
})
</script>
<style lang="scss" scoped>
.page-content {
  box-shadow: 0 0 6px 0 rgba(51, 51, 51, 0.08);
  margin-top: 10px;
  border-radius: 4px;
  padding: 15px 5px 15px 15px;
  background-color: #ffffff;
  .icon-container {
    display: flex;
    flex-wrap: wrap;
    .icon-item {
      border: 1px solid #dcdee2;
      text-align: center;
      width: 150px;
      height: 180px;
      margin-top: 10px;
      color: #515a6e;
      padding-top: 20px;
      border-radius: 4px;
      margin-right: 11.1px;
      position: relative;
    }
    .icon-name {
      color: #17233d;
      font-size: 14px;
      margin-top: 15px;
    }
    .icon-code {
      color: #515a6e;
      font-size: 14px;
      margin-top: 6px;
    }
    .icon-footer {
      border-top: 1px solid #dcdee2;
      padding: 12px 0px;
      font-size: 14px;
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      cursor: pointer;
      span:hover {
        color: #17233d;
      }
    }
  }
}
</style>
