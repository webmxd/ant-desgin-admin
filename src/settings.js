export default {
  /**
   * 页面标签的title
   */
  title: '管理系统',
  /**
   * 侧边栏菜单顶部标题，为空则只显示logo
   */
  siderLogoTitle: '后台管理系统',
  /**
   * 布局设置
   */
  // 侧边栏展开的宽度，单位px，不可动态修改，需要和 styles/variables.less中 @base-sider-width 保持一致
  siderWidth: 250,
  // 侧边栏收起的宽度，单位px，不可动态修改，需要和 styles/variables.less中 @base-sider-width 保持一致
  siderCollapsedWidth: 80,
  layout: {
    // 侧边栏主题色：深色主题dark，浅色主题light，默认深色dark
    siderTheme: 'dark',
    // 导航布局：侧边导航sider，混合导航mixed，默认侧边导航sider
    structure: 'sider',
    // 系统主题颜色
    themeColor: '#1677ff',
    // 是否固定顶栏
    fixedHeader: true,
    // 是否固定左侧栏
    fixedSider: true,
    // 是否显示左侧栏顶部的logo
    siderLogo: true,
    // 侧边菜单栏是否开启手风琴模式，开启后每次打开菜单只能打开一个
    menuAccordion: true,
    // 菜单折叠时，是否在子菜单显示父级菜单名称
    menuCollapseParentTitle: false,
    // 是否开启水印
    isWater: true,
    // 是否开启 tabsView
    tabsView: true,
    // tabsView标签显示图标
    tabsViewIcon: false,
    // topNav导航显示图标
    topNavIcon: true,
    // 面包屑第一层是否显示首页，否则会以当前点击的菜单根目录显示在第一层
    rootHome: true,
    // 显示页脚
    footer: true
  },
  /**
   * 是否显示动态标题
   */
  showDynamicTitle: true,
  /**
   * 系统主题颜色
   */
  themeColorList: [
    {
      label: '深蓝',
      value: '#1677ff'
    },
    {
      label: '薄暮',
      value: '#F5222D'
    },
    {
      label: '火山',
      value: '#FA541C'
    },
    {
      label: '日暮',
      value: '#FAAD14'
    },
    {
      label: '明青',
      value: '#13C2C2'
    },
    {
      label: '草绿',
      value: '#19be6b'
    },
    {
      label: '酱紫',
      value: '#722ED1'
    }
  ]
}
